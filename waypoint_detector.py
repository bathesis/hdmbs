import cv2 as cv
import numpy as np
import time
from picamera import PiCamera


class CircleDetector:

    def detect_waypoints(self, image, show):
        src = cv.imread(image, cv.IMREAD_COLOR)
        gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
        gray = cv.medianBlur(gray, 3)
        gray = cv.addWeighted(gray, 0.65, gray, 0, 1.2)

        waypoints = cv.HoughCircles(gray, cv.HOUGH_GRADIENT, 2, 15, param1=65, param2=24, minRadius=5, maxRadius=10)
        if show:
            self.show_waypoints(waypoints, src)

        return waypoints

    def zeroing(self, image, show, crop):
        crop -= 10
        src = cv.imread(image, cv.IMREAD_COLOR)
        gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
        gray = gray[crop:300, crop:300]

        piece = cv.HoughCircles(gray, cv.HOUGH_GRADIENT, 1, 7, param1=20, param2=10, minRadius=4, maxRadius=7)

        if show:
            self.show_waypoints(piece, gray)

        if piece is not None:
            piece[0, 0, 0] += crop
            piece[0, 0, 1] += crop

        return piece

    def detect_manual(self):
        camera = PiCamera()
        camera.resolution = (300, 300)
        time.sleep(1)
        camera.capture("test.jpg", resize=(300, 300))
        self.detect_waypoints("test.jpg", True)
        self.zeroing("test.jpg", True, 270)

    def show_waypoints(self, waypoints, image):
        if waypoints is not None:
            circles = np.uint16(np.around(waypoints))
            first_circle = circles[0][0]
            print(first_circle)
            cv.circle(image, (0, 0), 1, (255, 255, 255), -1)
            for i in circles[0, :]:
                center = (i[0], i[1])
                cv.circle(image, center, 1, (0, 100, 100), 1)
                radius = i[2]
                cv.circle(image, center, radius, (255, 0, 255), 1)

            center = (first_circle[0], first_circle[1])
            cv.circle(image, center, 1, (255, 255, 255), 1)
            radius = first_circle[2]
            cv.circle(image, center, radius, (255, 255, 255), 1)

        cv.imshow("waypoints", image)
        cv.waitKey(0)
        return 0

    def show_waypoints_alt(self, waypoints, image):
        if waypoints is not None:
            circles = np.uint16(np.around(waypoints))
            a = len(circles)
            for i in range(a):
                center = (circles[i][0][0], circles[i][0][1])
                cv.circle(image, center, 1, (0, 100, 100), 1)
                radius = circles[i][0][2]
                cv.circle(image, center, radius, (255, 0, 255), 1)

            cv.imshow("waypoints", image)
            cv.waitKey(0)
            return 0


if __name__ == "__main__":
    detect = CircleDetector()
    detect.detect_manual()

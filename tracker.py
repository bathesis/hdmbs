from picamera import PiCamera
import numpy as np
import waypoint_detector
import inference


class Tracker:

    def __init__(self, camera, waypoints, infer, color):
        self.inf = infer
        self.cam = camera
        self.pieces = {}
        self.labels = []
        self.waypoints = np.uint16(np.around(waypoints))[0]
        self.prev_occ = []
        self.color = color

    def get_obstacles(self):
        self.cam.capture("tracking.jpg")
        return self.inf.infer("tracking.jpg")

    def run(self, t):
        if t == 0:
            print(f"Getting first positions...")
        else:
            print(f"Processing Turn {t}. Please wait...")
        wps = {}
        new_occ = self.get_occupied_waypoints()
        if t == 0:
            self.prev_occ = new_occ
        else:
            for i, o in enumerate(self.prev_occ):
                if o == new_occ[i]:
                    continue
                else:
                    if not new_occ[i]:
                        wps['s'] = self.waypoints[i]
                    else:
                        wps['g'] = self.waypoints[i]
        self.prev_occ = new_occ
        print("Processing done.")
        return wps

    def get_occupied_waypoints(self):
        wp_occupied = []
        if self.waypoints is not None:
            self.pieces, b, self.labels = self.get_obstacles()
            for w in self.waypoints:
                occ = False
                for i, p in enumerate(self.pieces):
                    if self.labels[i] == self.color:
                        if self.on_waypoint(w[0], w[1], w[2], p[0], p[1]):
                            occ = True
                wp_occupied.append(occ)
        return wp_occupied

    def on_waypoint(self, wx, wy, r, px, py):
        return (px - wx)**2 + (py-wy)**2 <= r**2


if __name__ == "__main__":
    cam = PiCamera()
    cam.resolution = (300, 300)
    cam.capture("pathfinding.jpg")
    inf = inference.Detector()
    wp = waypoint_detector.CircleDetector()
    tr = Tracker(cam, wp.detect_waypoints("pathfinding.jpg", False), inf, "green")
    turn = 0
    tr.run(turn)
    turn += 1
    while True:
        input("Hit Enter after move")
        print(tr.run(turn))
        turn += 1

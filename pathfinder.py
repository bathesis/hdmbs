import numpy as np
import pyastar
import cv2 as cv
from picamera import PiCamera
from shapely.geometry import LineString
from shapely.geometry.polygon import LinearRing
import inference


class Pathfinder:

    def run(self, image, obstacles, start, goal, manual=False):
        self.img = cv.imread(image)
        self.img = cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)
        self.obstacles = obstacles
        self.weights = np.array(self.img).astype(dtype=np.float32)
        self.weights[self.weights >= 0] = 1
        travel_points = self.create_path(self.weights, start, goal)
        if manual:
            img = np.stack((self.img, self.img, self.img), axis=2)
            img[travel_points[:, 1], travel_points[:, 0]] = (0, 255, 0)
            cv.imshow("Path", img)
            cv.waitKey(0)
        print(f"Travel Points: {travel_points}")
        return travel_points

    def create_path(self, weights, start, goal):
        r = 16
        for o in self.obstacles:
            x = int(o[0])
            y = int(o[1])
            if not self.on_waypoint(x, y, 8, start[0], start[1]):
                px1 = int(x - r)
                py1 = int(y - r)
                px2 = int(x + r)
                py2 = int(y + r)
                cv.rectangle(weights, (py1, px1), (py2, px2), (10, 10, 10), -1)
                cv.rectangle(self.img, (px1, py1), (px2, py2), (10, 10, 10), -1)
        path = pyastar.astar_path(weights, start, goal, allow_diagonal=False)
        result = self.get_straight_connections(self.simplify_path(path))
        return result

    def simplify_path(self, path):
        simple_path = path
        checkpoint = simple_path[0]
        current_point = simple_path[1]
        for i, p in enumerate(path):
            if i + 1 < len(simple_path):
                if self.walkable(checkpoint, current_point):
                    current_point = simple_path[i + 1]
                    simple_path = np.delete(simple_path, i, axis=0)
                else:
                    checkpoint = simple_path[i]
                    current_point = simple_path[i + 1]
        return simple_path

    def get_straight_connections(self, path):
        point_path = path
        checkpoint = point_path[0]
        current_point = point_path[1]
        points_to_delete = []
        for h, p in enumerate(point_path):
            if h + 1 < len(point_path):
                if not self.check_los(checkpoint, current_point):
                    points_to_delete.append(h)
                    current_point = point_path[h + 1]
                else:
                    checkpoint = point_path[h]
                    current_point = point_path[h + 1]

        point_path = np.delete(point_path, points_to_delete, axis=0)
        return point_path

    def walkable(self, first, second):
        if self.weights[first].any() > 1:
            return False
        elif self.weights[second].any() > 1:
            return False
        else:
            return True

    def check_los(self, start, end):
        line = LineString([start, end])
        cv.line(self.img, (start[0], start[1]), (end[0], end[1]), (255, 255, 255), 1)
        coldect = []
        r = 16
        for c in self.obstacles:
            x = int(c[0])
            y = int(c[1])
            if not self.on_waypoint(x, y, 8, start[0], start[1]):
                px1 = x - r
                py1 = y - r
                px2 = x + r
                py2 = y + r
                px3 = x + r
                py3 = y - r
                px4 = x - r
                py4 = y + r
                coldect.append(LinearRing([(px1, py1), (px3, py3), (px2, py2), (px4, py4)]))

        for p in coldect:
            points = np.array(p.coords, np.int32)
            points = points.reshape((-1, 1, 2))
            cv.polylines(self.img, [points], False, (204, 0, 0), 1)

        collision = False

        for obs in coldect:
            if obs.crosses(line) or obs.intersects(line):
                collision = True
                break
            else:
                collision = False
                continue

        return collision

    def on_waypoint(self, wx, wy, r, px, py):
        return (px - wx) ** 2 + (py - wy) ** 2 <= r ** 2


if __name__ == "__main__":
    cam = PiCamera()
    cam.resolution = (300, 300)
    cam.capture("pathfinding.jpg")
    pf = Pathfinder()
    inf = inference.Detector()
    o, b, l = inf.infer("pathfinding.jpg")
    pf.run("pathfinding.jpg", o, (45, 133), (265, 177), True)

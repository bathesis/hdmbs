import sys
import time
import cv2 as cv
from picamera import PiCamera
from time import sleep
import numpy as np
from colorama import Fore, Back, Style
import asyncio
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.dispatcher import Dispatcher
from pythonosc.udp_client import SimpleUDPClient
from kbhit import KBHit
import argparse
import subprocess
# own scripts
import grbl_connector
import waypoint_detector
import measurement_calibration
import pathfinder
import inference
import tracker
from ledctrl import ledctrl

detect = waypoint_detector.CircleDetector()
magnet = grbl_connector.GrblConnector()
camera = PiCamera()
infer = inference.Detector()
pathfinder = pathfinder.Pathfinder()
kb = KBHit()
parse = argparse.ArgumentParser(description="Config Arguments")
led = ledctrl.LEDCtrl(12)


class main:

    def __init__(self):
        self.init_args()
        led.solid((255, 255, 255))
        magnet.init_grbl()
        self.x_home = True
        self.y_home = True
        self.x_zero = 57
        self.y_zero = 55
        self.X_OFFSET = 0
        self.Y_OFFSET = 0
        self.X_OFFSET_DIFF = 0
        self.Y_OFFSET_DIFF = 0
        self.right_corner = None
        self.RATIO = 1.378
        self.X_SHIFT = 0
        self.Y_SHIFT = 0
        self.mag = magnet
        self.waypoints = None
        camera.resolution = (300, 300)
        self.dispatcher = Dispatcher()
        self.client = SimpleUDPClient(self.client_ip, int(self.client_port))
        self.dispatcher.map("/setup/request", self.setup_send)
        self.dispatcher.map("/setup/answer", self.setup_receive)
        self.dispatcher.map("/move", self.move_piece)
        self.currentWP = 0
        self.track = None
        self.setup()

    def init_args(self):
        parse.add_argument("-s", type=str, dest="server_ip", required=True)
        parse.add_argument("-sp", type=str, dest="server_port", required=True)
        parse.add_argument("-c", type=str, dest="client_ip", required=True)
        parse.add_argument("-cp", type=str, dest="client_port", required=True)
        parse.add_argument("-p", type=str, dest="player_color", required=True)

        args = parse.parse_args()
        self.server_ip = args.server_ip
        self.server_port = args.server_port
        self.client_ip = args.client_ip
        self.client_port = args.client_port
        self.player_color = args.player_color
        print(
            f"Starting Table with client with color {self.player_color}. Client: {self.client_ip}, server: {self.server_ip}")

    async def init_main(self):
        server = AsyncIOOSCUDPServer((self.server_ip, int(self.server_port)), self.dispatcher, asyncio.get_event_loop())
        transport, protocol = await server.create_serve_endpoint()

        await self.main()

        transport.close()

    async def main(self):
        turn = 0
        self.track.run(turn)
        turn += 1
        print(f"{Fore.GREEN}Ready to play! Hit [m] to confirm first move.\n{Style.RESET_ALL}")
        while True:
            if kb.kbhit():
                c = kb.getch()
                if c == 'm':
                    new_points = self.track.run(turn)
                    if len(new_points) == 2:
                        self.send_move(new_points)
                    turn += 1
                    print(f"{Fore.GREEN}Hit [m] to confirm next move.\n{Style.RESET_ALL}")
                if c == 'q':
                    sys.exit(0)
                if c == 's':
                    self.send_offsets()
            await asyncio.sleep(0)

    def send_move(self, mp):
        xs = mp['s'][0]
        ys = mp['s'][1]
        xg = mp['g'][0]
        yg = mp['g'][1]
        arg = f"{xs},{ys},{xg},{yg}"
        self.client.send_message("/move", arg)

    def setup_send(self, address, *args):
        self.send_offsets()

    def send_offsets(self):
        arg = [int(self.X_OFFSET), int(self.Y_OFFSET)]
        self.client.send_message("/setup/answer", arg)
        print("Sent setup data to other table\n")

    def setup_receive(self, address, *args):
        print(f"Received offset from other table: {args}\n")
        self.X_OFFSET_DIFF = self.X_OFFSET - int(args[0])
        self.Y_OFFSET_DIFF = self.Y_OFFSET - int(args[1])
        print(f"Offset difference: x: {self.X_OFFSET_DIFF}, y: {self.Y_OFFSET_DIFF}")

    def setup(self):
        magnet.move(self.x_zero, self.y_zero, 2000, False, True)
        camera.capture("calib.jpg", resize=(300, 300))

        self.calibrate()

    def zero_system(self):
        self.x_zero = 21
        self.y_zero = 19
        x = self.right_corner[0]
        y = self.right_corner[1]
        input("Place calibration piece on board and hit [ENTER]\n")
        while self.x_home or self.y_home:
            camera.capture("zeroing.jpg", resize=(300, 300))
            zeroing_piece = detect.zeroing("zeroing.jpg", False, x)
            print("Searching zero position...")
            if zeroing_piece is not None:
                circles = np.uint16(np.around(zeroing_piece))
                for i in circles[0, :]:
                    center = (i[0], i[1])
                    if (x - 2) < center[0] < (x + 2):
                        print("X home found")
                        self.x_home = False
                    if (y - 2) < center[1] < (y + 2):
                        print("Y home found")
                        self.y_home = False
            self.x_zero -= 1
            self.y_zero -= 1
            magnet.move(self.x_zero, self.y_zero, 1000, True, False)
            sleep(0.5)
        magnet.set_work_offset(self.x_zero, self.y_zero)
        input("Setup complete. Place pieces on board and hit [ENTER]\n")
        asyncio.run(self.init_main())

    def move_piece(self, address, *args):
        print("Received move data from other table. Please wait...")
        print(f"Message received: {args}")
        camera.capture("pathfinding.jpg")
        obstacles, boxes, label = infer.infer("pathfinding.jpg")
        args = str(args[0]).split(',')
        x1 = int(args[0])
        y1 = int(args[1])
        x2 = int(args[2])
        y2 = int(args[3])
        print(f"Moving... Coordinates: {x1, y1, x2, y2}")
        self.calculate_move_dest(x1, y1, False, False)
        self.calculate_move(pathfinder.run("pathfinding.jpg", obstacles, (x1, y1), (x2, y2)))
        print(f"{Fore.GREEN}Hit [m] to confirm next move.\n{Style.RESET_ALL}")

    def demo_move(self, address, *args):
        if args == 0:
            return
        demo = cv.imread("zeroing.jpg", cv.IMREAD_COLOR)
        self.waypoints = np.uint16(np.around(self.waypoints))
        if self.currentWP < len(self.waypoints[0]):
            i = self.currentWP
            print(self.waypoints[0, i])
            x = self.waypoints[0, i, 0]
            y = self.waypoints[0, i, 1]
            r = self.waypoints[0, i, 2]
            cv.circle(demo, (x, y), r, (255, 0, 0), 1)
            cv.imshow("Moving...", demo)
            self.currentWP += 1
            self.calculate_move_dest(x, y, True, False)

    def calibrate(self):
        magnet.toggle_magnet(1)
        measure = measurement_calibration.MeasurementCalibration()
        self.RATIO, self.X_SHIFT, self.Y_SHIFT, self.right_corner = measure.calibrate("calib.jpg", 339)
        self.waypoints = detect.detect_waypoints("calib.jpg", False)
        self.X_OFFSET = 300 - self.right_corner[0]
        self.Y_OFFSET = 300 - self.right_corner[1]
        print(self.RATIO, self.X_SHIFT, self.Y_SHIFT, f"X Offset: {self.X_OFFSET}", f"Y Offset: {self.Y_OFFSET}")
        self.send_offsets()
        self.track = tracker.Tracker(camera, self.waypoints, infer, self.player_color)
        self.zero_system()

    def calculate_move(self, movepoints):
        for i, mp in enumerate(movepoints):
            m = i != len(movepoints) - 1
            self.calculate_move_dest(mp[0], mp[1], True, m)

    def calculate_move_dest(self, wp_x, wp_y, magnet_s, magnet_e):
        print(self.RATIO, self.X_SHIFT, self.Y_SHIFT, f"X Offset: {self.X_OFFSET}", f"Y Offset: {self.Y_OFFSET}")
        wp_x -= self.X_OFFSET_DIFF
        wp_y -= self.Y_OFFSET_DIFF
        print(f"Recalculated waypoints w OffsetDiff: {wp_x, wp_y}")
        new_x = abs(((300 - wp_y - self.Y_OFFSET - (wp_y * self.Y_SHIFT)) * self.RATIO))
        new_y = abs(((300 - wp_x - self.X_OFFSET - (wp_x * self.X_SHIFT)) * self.RATIO))
        magnet.move(new_x, new_y, 3000, magnet_s, magnet_e)

    def __del__(self):
        led.off()


if __name__ == "__main__":
    app = main()

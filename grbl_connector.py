import sys
import serial
import time

GRBL_RST = b'\x18'
GRBL_DISAL = b'$X\n'
GRBL_STAT = b'?'

GRBL_ERR_SL = "Soft limit"


class GrblConnector:

    def __init__(self):
        self.ser = serial.Serial('/dev/ttyACM0', baudrate=115200)
        self.debug_prefix = "Grbl Connector:"
        self.last_grbl_answer = 'none'

    def init_grbl(self):
        print(self.debug_prefix, "Initializing")
        self.ser.write(b'\r\n\r\n')
        time.sleep(2)
        self.ser.flushInput()
        self.home()

    def home(self):
        print(self.debug_prefix, "Homing...please wait")
        self.ser.write(b'$H\n')
        self.wait_for_grbl_answer()
        if self.last_grbl_answer == "ok":
            print(self.debug_prefix, "Homing completed!")
            self.set_work_offset(0, 0)
            return True
        else:
            print(self.debug_prefix, self.last_grbl_answer)
            return False

    def move(self, x, y, speed, magnet_start, magnet_end):
        print(self.debug_prefix, f"Coordinates are ({x};{y})")
        self.toggle_magnet(magnet_start)
        command = f"G1X{x}Y{y}F{speed}\n"
        command = bytes(command, "utf-8")
        self.ser.write(command)
        self.wait_for_grbl_answer()
        if self.last_grbl_answer == "ok":
            print(self.debug_prefix, f"moving to ({x};{y}), speed: {speed}")
        else:
            self.process_error(self.last_grbl_answer)
            return False
        while self.get_status() != "Idle":
            time.sleep(0.2)
        print(self.debug_prefix, "position reached")
        self.toggle_magnet(magnet_end)
        return True

    def move_seq(self, positions, speed):
        self.toggle_magnet(True)
        for p in positions:
            x = p[0]
            y = p[1]
            print(self.debug_prefix, f"Coordinates are ({x};{y})")
            command = f"G1X{x}Y{y}F{speed}\n"
            command = bytes(command, "utf-8")
            self.ser.write(command)
            self.wait_for_grbl_answer()
            if self.last_grbl_answer == "ok":
                print(self.debug_prefix, f"moving to ({x};{y}), speed: {speed}")
            else:
                self.process_error(self.last_grbl_answer)
                break
        self.toggle_magnet(False)

    def set_work_offset(self, x, y):
        command = f"G10P0L20X0Y0\n"
        command = bytes(command, "utf-8")
        self.ser.write(command)
        self.wait_for_grbl_answer()

    def toggle_magnet(self, on):
        if on:
            self.ser.write(b'M3\n')
            self.wait_for_grbl_answer()
            print(self.debug_prefix, "Magnet on")
        else:
            self.ser.write(b'M5\n')
            self.wait_for_grbl_answer()
            print(self.debug_prefix, "Magnet off")

    def wait_for_grbl_answer(self):
        grbl_feedback = self.ser.readline()
        grbl_feedback = grbl_feedback.strip().decode("utf-8")
        self.last_grbl_answer = grbl_feedback

    def get_status(self):
        self.ser.write(GRBL_STAT)
        ans = self.ser.readline().strip().decode("utf-8")
        return ans.replace('<', '').split(sep=',')[0]

    def get_position(self):
        self.ser.write(GRBL_STAT)
        ans = self.ser.readline().strip()
        print(ans)

    def process_error(self, error):
        print(self.debug_prefix, error)
        try:
            err = error.split(':')[1].strip()
            if err == GRBL_ERR_SL:
                self.reset_and_unlock()
        except IndexError:
            print("Unexpected error:", sys.exc_info()[0])
            # self.reset_and_unlock()

    def reset_and_unlock(self):
        self.ser.write(GRBL_RST)
        self.ser.write(GRBL_DISAL)
        time.sleep(1)
        self.ser.flushInput()
        print(self.debug_prefix, "reset and unlocked.")

    def shutdown(self):
        if self.ser is not None and self.ser.is_open:
            self.toggle_magnet(0)
            self.ser.close()
            print(self.debug_prefix, "Closed serial port")

    def __del__(self):
        self.shutdown()


if __name__ == "__main__":
    test = GrblConnector()
    test.move_seq([(140, 80), (200, 170), (10, 10)], 3000)

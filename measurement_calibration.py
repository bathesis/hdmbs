import cv2
import cv2 as cv
import numpy as np
from picamera import PiCamera
from time import sleep

Y_CROP_BT = (278, 284)
X_CROP_BT = (20, 295)
Y_CROP_TP = (25, 45)
X_CROP_TP = (268, 274)


class MeasurementCalibration:

    def calibrate_manual(self, measurement):
        camera = PiCamera()
        camera.resolution = (300, 300)
        sleep(1)
        camera.capture("test.jpg")
        self.load_image("test.jpg")
        self.calculate_ratios(self.find_corners(True), self.find_upper_corner(True), measurement, True)
        cv.waitKey(0)

    def calibrate(self, image, measurement):
        self.load_image(image)
        ratio, x_shift, y_shift, right_corner = self.calculate_ratios(self.find_corners(False),
                                                                      self.find_upper_corner(False), measurement, False)
        return ratio, x_shift, y_shift, right_corner

    def load_image(self, image):
        self.src = cv.imread(image)
        self.gray = cv.cvtColor(self.src, cv.COLOR_BGR2GRAY)

    def find_corners(self, debug):
        gray_crop = self.gray[Y_CROP_BT[0]:Y_CROP_BT[1], X_CROP_BT[0]:X_CROP_BT[1]]

        corners = cv.goodFeaturesToTrack(gray_crop, 2, 0.01, 10)
        corners = np.int0(corners)

        for i in corners:
            x, y = i.ravel()
            cv.circle(gray_crop, (x, y), 1, 255, -1)

        mapped = corners.copy()

        for i in mapped:
            i[0, 0] += X_CROP_BT[0]
            i[0, 1] += Y_CROP_BT[0]

        if debug:
            for i in corners:
                x, y = i.ravel()
                cv.circle(gray_crop, (x, y), 1, (255, 255, 255), -1)
                cv.circle(gray_crop, (x, y), 1, (255, 255, 255), -1)

            for i in mapped:
                x, y = i.ravel()
                print(f"Lower Corner: {x};{y}")
                cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)
                cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)

            cv2.imshow('mapped', self.src)
            cv2.imshow('lower corners', gray_crop)

        return mapped

    def find_upper_corner(self, debug):
        gray = self.gray[Y_CROP_TP[0]:Y_CROP_TP[1], X_CROP_TP[0]:X_CROP_TP[1]]
        upper_corners = cv.goodFeaturesToTrack(gray, 1, 0.01, 10)
        if upper_corners is not None:
            upper_corners = np.int0(upper_corners)
            for i in upper_corners:
                x, y = i.ravel()
                cv.circle(gray, (x, y), 1, 255, -1)

            mapped = upper_corners.copy()

            for i in mapped:
                i[0, 0] += X_CROP_TP[0]
                i[0, 1] += Y_CROP_TP[0]

            if debug:
                for i in upper_corners:
                    x, y = i.ravel()
                    cv.circle(gray, (x, y), 1, (255, 255, 255), -1)
                    cv.circle(gray, (x, y), 1, (255, 255, 255), -1)

                for i in mapped:
                    x, y = i.ravel()
                    print(f"Upper Corner: {x};{y}")
                    cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)
                    cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)

                cv2.imshow('mapped', self.src)
                cv2.imshow('upper corners', gray)

            return mapped

    def debug(self, corners, img):
        for i in corners:
            x, y = i.ravel()
            print(f"Lower Corner: {x};{y}")
            cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)
            cv.circle(self.src, (x, y), 1, (255, 255, 255), -1)

        cv2.imshow('mapped', self.src)
        cv2.imshow('corners', img)

    def calculate_ratios(self, lower_corners, upper_corner, measured_dist, debug):
        index = np.argmax([lower_corners[0, 0, 0], lower_corners[1, 0, 0]])
        distance_x = abs(lower_corners[index, 0, 1] - upper_corner[0, 0, 1])
        distance_y = abs(lower_corners[0, 0, 0] - lower_corners[1, 0, 0])
        x_shift = (lower_corners[index, 0, 0] - upper_corner[0, 0, 0]) / distance_x
        y_shift = (lower_corners[0, 0, 1] - lower_corners[1, 0, 1]) / distance_y

        real_dist = measured_dist / distance_y

        right_corner = lower_corners[index, 0]

        if debug:
            print("Pixel Distance: " + str(distance_y))
            print("1 mm == " + str(real_dist) + " px")
            print(f"X Shift: {x_shift}, Y Shift: {y_shift}")

        return real_dist, x_shift, y_shift, right_corner


if __name__ == "__main__":
    calib = MeasurementCalibration()
    calib.calibrate_manual(measurement=339)

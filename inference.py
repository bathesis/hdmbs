import tensorflow as tf
import cv2 as cv
import time
import numpy as np
import matplotlib.pyplot as plt
from picamera import PiCamera


class Utils:

    def calculate_center(self, box):
        return int(((box[1] + box[3]) / 2) * 300), int(((box[0] + box[2]) / 2) * 300)

    def draw_rect(self, image, box):
        y_min = int(max(1, (box[0] * 300)))
        x_min = int(max(1, (box[1] * 300)))
        y_max = int(min(300, (box[2] * 300)))
        x_max = int(min(300, (box[3] * 300)))

        cv.rectangle(image, (x_min, y_min), (x_max, y_max), (255, 0, 0), 2)
        return [x_min, y_min, x_max, y_max]


class Detector:

    def __init__(self):
        print("Preparing inference...")
        self.saved_model_dir = "training/model/tflite/test.tflite"
        self.CLASSES = {0: 'black', 1: 'green'}
        self.THRESHOLD = 0.5
        self.interpreter = tf.lite.Interpreter(model_path=self.saved_model_dir)
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()
        self.interpreter.allocate_tensors()
        self.ut = Utils()

    def infer(self, image):
        img = cv.imread(image)
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        img_data = cv.resize(img, (640, 640))
        input_mean = 127.5
        input_std = 127.5
        img_data = (np.float32(img_data) - input_mean) / input_std

        self.interpreter.set_tensor(self.input_details[0]['index'], [img_data])

        self.interpreter.invoke()

        rects = self.interpreter.get_tensor(self.output_details[0]['index'])
        scores = self.interpreter.get_tensor(self.output_details[2]['index'])
        classes = self.interpreter.get_tensor(self.output_details[1]['index'])

        pieces = []
        boxes = []
        labels = []

        for i, d in enumerate(scores[0]):
            if d > self.THRESHOLD:
                label = self.CLASSES[classes[0][i]]
                box = rects[0][i]
                center = self.ut.calculate_center(box)
                cv.circle(img, center, 1, (255, 255, 255), 1)
                boxes.append(self.ut.draw_rect(img, box))
                pieces.append(center)
                labels.append(label)
                cv.putText(img, label + str(round(d, 3)), (int(box[1] * 300), int(box[0] * 300 - 5)), cv.FONT_HERSHEY_PLAIN, 0.75,
                           (255, 255, 255), 1)

        #img = np.rot90(img, 3)
        #plt.imshow(img)
        #plt.show()
        return pieces, boxes, labels


if __name__ == "__main__":
    print("Starting inference...")
    start_time = time.time()
    de = Detector()
    cam = PiCamera()
    cam.resolution = (300, 300)
    cam.capture("inf.jpg")
    print(de.infer("inf.jpg"))
    end_time = time.time()
    elapsed_time = end_time - start_time
    print('Done! Took {} seconds'.format(elapsed_time))

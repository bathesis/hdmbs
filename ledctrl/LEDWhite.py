import board
import neopixel

pixels = neopixel.NeoPixel(board.D18, 12)

for p in range(12):
    pixels[p] = (255, 255, 255)

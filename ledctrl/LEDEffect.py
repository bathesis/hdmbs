import argparse
import board
import neopixel
from time import sleep

parse = argparse.ArgumentParser(description="Config Arguments")
pixels = neopixel.NeoPixel(board.D18, 12)
off = (0, 0, 0)
white = (255, 255, 255)
modes = {'processing': (0, 128, 128), 'moving': (255, 128, 0), 'camera': (255, 0, 0)}


def run():
    parse.add_argument("-m", type=str, dest="mode", required=True)
    args = parse.parse_args()
    mode = args.mode
    if mode in modes:
        loading(modes[mode], 0.1)
    else:
        loading((255, 0, 0), 0.02)


def loading(color, speed):
    for p in range(12):
        pixels[p] = white
    while True:
        for p in range(12):
            if p == 0:
                pixels[p] = color
                pixels[11] = white
            else:
                pixels[p] = color
                pixels[p - 1] = white
            sleep(speed)


if __name__ == "__main__":
    run()

import board
import neopixel

pixels = neopixel.NeoPixel(board.D18, 12)

for p in range(12):
    pixels[p] = (0, 0, 0)

import board
import neopixel
from time import sleep

OFF = (0, 0, 0)
WHITE = (255, 255, 255)


class LEDCtrl:

    def __init__(self, pixel_count):
        self.pixel_count = pixel_count
        self.pixels = neopixel.NeoPixel(board.D18, pixel_count)

    def solid(self, color, brightness=1.0):
        self.pixels.brightness = brightness
        self.pixels.fill(color)

    def off(self):
        for p in range(self.pixel_count):
            self.pixels[p] = (0, 0, 0)

    def loading(self, color, speed):
        for p in range(12):
            self.pixels[p] = WHITE
        while True:
            for p in range(12):
                if p == 0:
                    self.pixels[p] = color
                    self.pixels[11] = WHITE
                else:
                    self.pixels[p] = color
                    self.pixels[p - 1] = WHITE
                sleep(speed)

import numpy as np
import cv2 as cv

mtx = np.array([[314.1792897, 0., 155.88066983],
                [0., 314.28304973, 151.18516425],
                [0., 0., 1.]])

dist = np.array([[2.65631958e-02, 7.75294255e-01, 1.60780376e-03, 4.63239397e-03, -2.73196638e+00]])


class Undistorter:

    def process_image(self, image):
        img = cv.imread(image)
        h, w = img.shape[:2]

        newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

        dst = cv.undistort(img, mtx, dist, None, newcameramtx)
        x, y, w, h = roi
        dst = dst[y:y + h, x:x + w]
        dst = cv.resize(dst, (300, 300))
        cv.imwrite("result.png", dst)
        return dst


if __name__ == "__main__":
    test = Undistorter()
    test.process_image("calib.jpg")

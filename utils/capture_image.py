from picamera import PiCamera
import cv2 as cv

camera = PiCamera()
camera.resolution = (300,300)

camera.start_preview(fullscreen=False, window=(100,20,300,300))

for i in range(25):
    camera.capture(f"snaps/{i}.jpg")
    print(f"Saved picture {i}")
    input("Press enter for next image")

camera.stop_preview()
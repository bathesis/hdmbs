import grbl_connector
import picamera
from time import sleep
import random

magnet = grbl_connector.GrblConnector()
camera = picamera.PiCamera()


class DataGenerator:

    def __init__(self, max_x, max_y, min_x, min_y):
        self.max_x = max_x
        self.max_y = max_y
        self.min_x = min_x
        self.min_y = min_y
        self.y = 0
        self.x = 0
        camera.resolution = (300, 300)
        camera.start_preview(fullscreen=False, window=(100, 20, 300, 300))

    def grid_data(self, dist):
        stops_y = self.max_y / dist
        stops_x = self.max_x / dist
        for i in range(int(stops_x) + 1):
            self.x = dist * i
            # print(self.x, self.y)
            for j in range(int(stops_y) + 1):
                self.y = dist * j
                print(self.x, self.y)
                self.move_and_capture(self.x, self.y, f"training/green/{i}_{j}.jpg")
        camera.stop_preview()

    def random_data(self, count):
        for i in range(count):
            x = random.randrange(self.min_x, self.max_x)
            y = random.randrange(self.min_y, self.max_y)
            self.move_and_capture(x, y, f"training/black/{i}.jpg")

        camera.stop_preview()

    def manual_data(self, count):
        for i in range(count):
            self.move_and_capture(0, 0, f"training/test/man_{i}.jpg")
            input("Press enter for next img")
        camera.stop_preview()

    def move_and_capture(self, x, y, filename):
        magnet.move(x + self.min_x, y + self.min_y, 4000, True, False)
        camera.capture(filename)
        print(f"Captured image {filename}")
        sleep(0.5)

    def __del__(self):
        camera.stop_preview()


if __name__ == "__main__":
    dg = DataGenerator(330, 330, 13, 13)
    dg.manual_data(15)
